" Configuration

if &compatible
  " Vim defaults to `compatible` when selecting a vimrc with the command-line
  " `-u` argument. Override this.
  set nocompatible
endif

" Environment

let $MYVIMDIR=expand("<sfile>:h")
source $MYVIMDIR/xdg.vim

" Automatic installation of vim-plug

set runtimepath+=~/.vim

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Manage plugins using vim-plug

call plug#begin('~/.vim/plugged')
Plug 'altercation/vim-colors-solarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'dense-analysis/ale'
Plug 'machakann/vim-highlightedyank'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-gitgutter'
Plug 'sheerun/vim-polyglot'
Plug 'preservim/nerdtree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
call plug#end()

" Basic settings
filetype plugin indent on

" Disable modifyOtherKeys
let &t_TI = ""
let &t_TE = ""

" Configure vim-airline theme
let g:airline_powerline_fonts = 1
let g:airline_theme='luna'
let g:airline_solarized_bg='light'

" Configure Solarized colorscheme, make sure to configure terminal's
" emulator colorscheme to use the Solarized palette
syntax enable
set background=light
if !has('gui_running')
  let g:solarized_termtrans=1
endif
silent! colorscheme solarized

set undofile

" prevents security exploits dealing with modelines in files
set modelines=0

" tab settings
set expandtab
set shiftwidth=0
set softtabstop=-1
set tabstop=4

set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2

set number
set relativenumber
set clipboard+=unnamedplus
set ignorecase
set smartcase
set incsearch
set showmatch
set wrap
set linebreak
set nolist
set spell spelllang=en_us

" Set undercurl mode, to fix highlight of misspelled words
set t_Cs=

" Mappings and shortcuts

" Silent version of the super user edit, sudo tee trick.
cnoremap W!! execute 'silent! write !sudo /usr/bin/tee "%" >/dev/null' <bar> edit!
" Talkative version of the super user edit, sudo tee trick.
cmap w!! w !sudo /usr/bin/tee >/dev/null "%"

" Basics

" inoremap jk <ESC>
let mapleader = ","

" Arrows are unvimlike

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" Control shortcuts

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
