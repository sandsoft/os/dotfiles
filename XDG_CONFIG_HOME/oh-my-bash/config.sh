#!/usr/bin/env bash

OSH_THEME="font"
OMB_USE_SUDO=true
completions=(
  git
  composer
  ssh
)

aliases=(
  general
)

plugins=(
  bashmarks
  git
  kubectl
)
