# sh/profile.sh - session for POSIX shells
# Detect OS -------------------------------------------------------------------
OS="$(uname -s)"
if [ "Linux" = "${OS}" ]; then
  if [ -f /etc/lsb-release ] || [ -d /etc/lsb-release.d ]; then
    DISTRO="$(lsb_release -i | cut -d: -f2 | sed s/'^\t'//)"
    export DISTRO
    DISTROVER="$(lsb_release -sr 2>/dev/null)"
    export DISTROVER
  else
    DISTRO="$(ls -d /etc/[A-Za-z]*[_-][rv]e[lr]* |
      grep -v "lsb" | sed 1q |
      cut -d '/' -f3 | cut -d '-' -f1 | cut -d '_' -f1)"
    export DISTRO
  fi
fi
export OS


# HOMEBREW

if [ "${OS}" = "Darwin" ]; then
    platform="$(uname -m)"
    if [ "${platform}" = "arm64" ]; then
      # ARM macOS
      HOMEBREW_PREFIX="/opt/homebrew"
    fi
    if [ "${platform}" = "x86_64" ]; then
      # Intel macOS
      HOMEBREW_PREFIX="/usr/local"
    fi
fi;

if [ "${OS}" = "Linux" ]; then
  HOMEBREW_PREFIX="/home/linuxbrew/.linuxbrew"
fi;

if [ -n "${HOMEBREW_PREFIX:-}" ]; then
  PATH="${HOMEBREW_PREFIX}/bin:${PATH}"
fi


# ENVIRONMENT -----------------------------------------------------------------
#XDG vars are the scaffold for the other ones, and quite important.
#Set them to their default values here
set -a
if [ "${OS}" = Darwin ]; then
  XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/Library/Application Support}"
  XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/Library/Caches}"
  MACOS_LIBRARY="${MACOS_LIBRARY:-$HOME/Library}"
  PREFIX="${PREFIX:-$HOME/Library/Local}"
  eval "$(locale)"
  . "${XDG_CONFIG_HOME}/environment.d/20-darwin.conf"
else
  XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
  XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"
  PREFIX="${PREFIX:-$HOME/.local}"
fi
XDG_BIN_HOME="${XDG_BIN_HOME:-$PREFIX/bin}"
XDG_DATA_HOME="${XDG_DATA_HOME:-$PREFIX/share}"
. "${XDG_CONFIG_HOME}/environment.d/10-applications.conf"

if command -v python3 >/dev/null 2>&1; then
  PATH="${PATH}:$(python3 -m site --user-base)/bin"
fi

PATH="${JENV_ROOT}/bin:${PATH}"
PATH="${PYENV_ROOT}/shims:${PYENV_ROOT}/bin:${PATH}"
PATH="${GCLOUD_SDK_DIR}/bin:${PATH}"

set +a
