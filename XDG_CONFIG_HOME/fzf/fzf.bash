# Setup fzf
# ---------

# Auto-completion
# ---------------
[[ $- == *i* ]] && . "${XDG_CONFIG_HOME}/fzf/completion.bash" 2> /dev/null

# Key bindings
# ------------
. "${XDG_CONFIG_HOME}/fzf/key-bindings.bash"
