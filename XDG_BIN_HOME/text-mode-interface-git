#!/usr/bin/env sh
# Tig: text-mode interface for Git

set -eu

V_TIG=2.5.5

command_exists() { command -v "$@" >/dev/null 2>&1; }

install() {
  tmpdir="$(mktemp -d)"
  git clone \
    --depth 1 \
    --branch "tig-${V_TIG}" \
    https://github.com/jonas/tig.git \
    "${tmpdir}"
  cd "${tmpdir}"
  make prefix="${PREFIX}"
  sudo make install prefix="${PREFIX}"
}

text_mode_interface_git() {
  if ! command_exists tig; then
    install
  fi
  tig_version="$(tig --version | grep -Eo '[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+' | head -1)"
  if [ "${tig_version}" != "${V_TIG}" ]; then
    install
  fi
  exec tig "$@"
}

text_mode_interface_git "$@"
