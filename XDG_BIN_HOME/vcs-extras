#!/usr/bin/env sh
#vcs-extras - little git extras

set -eu

V_GIT_EXTRAS=7.3.0

command_exists() { command -v "$@" >/dev/null 2>&1; }

install() {
  tmpdir="$(mktemp -d)"
  git clone \
    --depth 1 \
    --branch "${V_GIT_EXTRAS}" \
    https://github.com/tj/git-extras.git \
    "${tmpdir}"
  cd "${tmpdir}"
  sudo make install PREFIX="${PREFIX}"
}

vcs_extras() {
  # git bulk stores workspaces in your git config
  # If ~/.gitconfig does not exist, changes will be stored in ${XDG_CONFIG_HOME/git/config
  # To prevent this, make sure ~/.gitconfig exists
  touch ~/.gitconfig
  if ! command_exists git-extras; then
    install
  fi
  git_extras_version="$(git-extras --version)"
  if [ "${git_extras_version}" != "${V_GIT_EXTRAS}" ]; then
    install
  fi
  exec git-extras "$@"
}

vcs_extras "$@"
