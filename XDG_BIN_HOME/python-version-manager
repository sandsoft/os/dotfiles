#!/usr/bin/env sh

set -eux

V_PYENV=2.2.3

command_exists() { command -v "$@" >/dev/null 2>&1; }

install() {
  case "$(uname -s)" in
  Linux)
    sudo apt-get install -y make build-essential libssl-dev zlib1g-dev \
      libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
      libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
    ;;
  Darwin)
    # install Xcode Command Line Tools (xcode-select --install)
    brew install openssl readline sqlite3 xz zlib
    ;;
  esac

  sudo rm -rdf "${XDG_DATA_HOME}/pyenv"
  git clone \
    --depth 1 \
    --branch "v${V_PYENV}" \
    https://github.com/pyenv/pyenv.git "${XDG_DATA_HOME}/pyenv"
}

python_version_manager() {
  if ! command_exists pyenv; then
    install
  fi
  pyenv_version="$(pyenv --version | grep -Eo '[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+')"
  if [ "${pyenv_version}" != "${V_PYENV}" ]; then
    install
  fi
  exec pyenv "$@"
}

python_version_manager "$@"
