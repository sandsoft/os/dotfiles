#.profile - {a,}sh startup file
# log out and back in again to verify changes made in this or any other sourced file(s).

PATH="$XDG_BIN_HOME:$PATH" #Fix for Alpine (?) resetting PATH
[[ -n "$OS" ]] || . "${XDG_CONFIG_HOME:-$HOME/.config}/sh/profile.sh"
[[ -f "$XDG_CONFIG_HOME/sh/rc.sh" ]] && . "$XDG_CONFIG_HOME/sh/rc.sh"
